package com.daniel.webmotors_cars_android.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.daniel.webmotors_cars_android.data.model.Car
import com.daniel.webmotors_cars_android.data.model.ResponseCars
import com.daniel.webmotors_cars_android.data.repository.CarsRepositoryImplementation
import com.daniel.webmotors_cars_android.domain.usecase.UseCaseGetCars
import com.daniel.webmotors_cars_android.domain.usecase.UseCaseGetListCarsFromLocalDatabase
import com.daniel.webmotors_cars_android.domain.usecase.UseCaseGetUpdateListCars
import com.daniel.webmotors_cars_android.domain.usecase.UseCaseSaveAllOnLocalDatabase
import com.daniel.webmotors_cars_android.presentation.utils.Event
import kotlinx.coroutines.launch

class CarsViewModel(private val getCarsUseCase: UseCaseGetCars,
                    private val getListCarsFromLocalDatabaseUseCase: UseCaseGetListCarsFromLocalDatabase,
                    private val getUpdateListCarsUseCase: UseCaseGetUpdateListCars,
                    private val saveAllOnLocalDatabaseUseCase: UseCaseSaveAllOnLocalDatabase): ViewModel() {

    var mutableLiveDataListUpdateCars : MutableLiveData<Event<ResponseCars>> = MutableLiveData<Event<ResponseCars>>()
    var mutableLiveErrorGetCars : MutableLiveData<Event<String>> = MutableLiveData<Event<String>>()
    var mutableLiveDataGetCars: MutableLiveData<Event<ArrayList<Car>>> = MutableLiveData<Event<ArrayList<Car>>>()

    private var page: Int = 1

    private var fragmentCallInitRequest: Boolean = true

    fun isFragmentCallInitRequest(): Boolean {
        return this.fragmentCallInitRequest
    }

    fun setFragmentCallInitRequest() {
        this.fragmentCallInitRequest = false
    }

    fun updateNextPage() {
        this.page += 1
    }

    fun getCurrentPage(): Int {
        return this.page
    }

    fun getCars() = viewModelScope.launch {
        val listCars = getCarsUseCase.makeAction()
        mutableLiveDataGetCars.postValue(Event(listCars))
    }

    fun getCarsFromLocalDatabase() = viewModelScope.launch {
        val listCars =  getListCarsFromLocalDatabaseUseCase.makeAction()
        mutableLiveDataGetCars.postValue(Event(listCars))
    }

    fun getUpdateCars() = viewModelScope.launch {
        val responseCars = getUpdateListCarsUseCase.makeAction(getCurrentPage())

        when (responseCars.code()) {
            200, 201, 202 -> {
                val responseListCarsBody = responseCars.body()!!
                saveAllOnLocalDatabaseUseCase.makeAction(responseListCarsBody)
                mutableLiveDataListUpdateCars.postValue(Event(responseListCarsBody))
            } else -> {
                mutableLiveErrorGetCars.postValue(Event("Ocorreu um erro. Por favor, tente atualizar novamente."))
            }
        }
    }
}
