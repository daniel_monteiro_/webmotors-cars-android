package com.daniel.webmotors_cars_android.presentation.view

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.daniel.webmotors_cars_android.R
import com.daniel.webmotors_cars_android.data.model.Car
import com.daniel.webmotors_cars_android.databinding.FragmentDetailCarBinding
import kotlinx.android.synthetic.main.grid_item_row.*
import kotlinx.android.synthetic.main.grid_item_row.view.*

/**
 * A simple [Fragment] subclass.
 * Use the [DetailCarFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DetailCarFragment : Fragment() {

    lateinit var detailCarFragmentBinding: FragmentDetailCarBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val carDetail = requireArguments().getSerializable("car") as Car
        setValuesOnScreen(carDetail)
    }

    @SuppressLint("SetTextI18n")
    private fun setValuesOnScreen(car: Car) {
        detailCarFragmentBinding.apply {
            Glide.with(this@DetailCarFragment.requireContext())
                .load(car.image)
                .placeholder(ContextCompat.getDrawable(this@DetailCarFragment.requireContext(), R.drawable.little_car))
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .into(ivCarDetail)
            tvVersion.text = car.version
            tvMakeModelCar.text = car.make + " | " + car.model
            tvKmCar.text = car.kM.toString() + " KM"
            tvPrice.text = "R$ " + car.price
            tvColor.text = car.color
            tvYearFabYearModel.text = "Year: " + car.yearModel + " | Year Fab: " + car.yearFab
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        detailCarFragmentBinding = FragmentDetailCarBinding.inflate(inflater, container, false)
        return detailCarFragmentBinding.root
    }
}