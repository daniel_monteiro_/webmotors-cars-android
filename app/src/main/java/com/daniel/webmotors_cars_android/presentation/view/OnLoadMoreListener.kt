package com.daniel.webmotors_cars_android.presentation.view

interface OnLoadMoreListener {
    fun onLoadMore()
}