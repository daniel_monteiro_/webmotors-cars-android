package com.daniel.webmotors_cars_android.data.model


import com.daniel.webmotors_cars_android.data.database.model.CarCache
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Car(
    @SerializedName("Color")
    val color: String,
    @SerializedName("ID")
    val iD: Int,
    @SerializedName("Image")
    val image: String,
    @SerializedName("KM")
    val kM: Int,
    @SerializedName("Make")
    val make: String,
    @SerializedName("Model")
    val model: String,
    @SerializedName("Price")
    val price: String,
    @SerializedName("Version")
    val version: String,
    @SerializedName("YearFab")
    val yearFab: Int,
    @SerializedName("YearModel")
    val yearModel: Int
): Serializable {
    fun getCarForCache(): CarCache {
        val carCache = CarCache(this.iD, this.color, this.image, this.kM, this.make, this.model,
            this.price, this.version, this.yearFab, this.yearModel)
        return carCache
    }
}