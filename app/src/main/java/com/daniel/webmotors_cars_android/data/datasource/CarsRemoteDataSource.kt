package com.daniel.webmotors_cars_android.data.datasource

import com.daniel.webmotors_cars_android.data.api.CarsApiService
import com.daniel.webmotors_cars_android.data.model.ResponseCars
import retrofit2.Response

class CarsRemoteDataSource(private val carsApiService: CarsApiService) {

    suspend fun getCars(page: Int): Response<ResponseCars> {
        return carsApiService.getCars(page)
    }
}