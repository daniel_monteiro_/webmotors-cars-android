package com.daniel.webmotors_cars_android.data.database.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.daniel.webmotors_cars_android.data.model.Car

@Entity(tableName = "car_cache_table")
class CarCache (
    @PrimaryKey @ColumnInfo(name = "id")
    val id: Int,
    @ColumnInfo(name = "color")
    val color: String,
    @ColumnInfo(name = "image")
    val image: String,
    @ColumnInfo(name = "km")
    val kM: Int,
    @ColumnInfo(name = "make")
    val make: String,
    @ColumnInfo(name = "model")
    val model: String,
    @ColumnInfo(name = "price")
    val price: String,
    @ColumnInfo(name = "version")
    val version: String,
    @ColumnInfo(name = "yearFab")
    val yearFab: Int,
    @ColumnInfo(name = "yearModel")
    val yearModel: Int
) {
    fun getCarConverted(): Car {
        val car = Car(this.color, this.id, this.image, this.kM, this.make
        , this.model, this.price, this.version, this.yearFab, this.yearModel)
        return car
    }
}