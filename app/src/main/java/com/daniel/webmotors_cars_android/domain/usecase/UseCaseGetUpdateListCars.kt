package com.daniel.webmotors_cars_android.domain.usecase

import com.daniel.webmotors_cars_android.data.model.ResponseCars
import com.daniel.webmotors_cars_android.domain.repository.CarsRepository
import retrofit2.Response

class UseCaseGetUpdateListCars(private val carsRepository: CarsRepository) {
    suspend fun makeAction(page: Int): Response<ResponseCars> {
        return carsRepository.getUpdateListCars(page)
    }
}