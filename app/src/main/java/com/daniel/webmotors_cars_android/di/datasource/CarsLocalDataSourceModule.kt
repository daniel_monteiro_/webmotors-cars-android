package com.daniel.webmotors_cars_android.di.datasource

import com.daniel.webmotors_cars_android.data.database.dao.CarsCacheDAO
import com.daniel.webmotors_cars_android.data.datasource.CarsLocalDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

@Module
@InstallIn(ApplicationComponent::class)
class CarsLocalDataSourceModule {

    @Provides
    fun provideCarsLocalDataSource(carsCacheDAO: CarsCacheDAO): CarsLocalDataSource {
        return CarsLocalDataSource(carsCacheDAO)
    }
}