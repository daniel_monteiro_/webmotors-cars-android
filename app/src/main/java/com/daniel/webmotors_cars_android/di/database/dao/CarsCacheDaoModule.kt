package com.daniel.webmotors_cars_android.di.database.dao

import com.daniel.webmotors_cars_android.data.database.CarsRoomDatabase
import com.daniel.webmotors_cars_android.data.database.dao.CarsCacheDAO
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

@Module
@InstallIn(ApplicationComponent::class)
class CarsCacheDaoModule {
    
    @Provides
    fun provideCarsCacheDao(database: CarsRoomDatabase): CarsCacheDAO {
        return database.carsCacheDao()
    }
    
}