package com.daniel.webmotors_cars_android.di.datasource

import com.daniel.webmotors_cars_android.data.api.CarsApiService
import com.daniel.webmotors_cars_android.data.datasource.CarsRemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

@Module
@InstallIn(ApplicationComponent::class)
class CarsRemoteDataSourceModule {
    @Provides
    fun provideCarsRemoteDataSource(carsApiService: CarsApiService):CarsRemoteDataSource {
        return CarsRemoteDataSource(carsApiService)
    }
}