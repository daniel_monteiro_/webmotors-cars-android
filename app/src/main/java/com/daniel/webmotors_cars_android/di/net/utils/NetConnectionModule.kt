package com.daniel.webmotors_cars_android.di.net.utils

import android.content.Context
import com.daniel.webmotors_cars_android.data.utils.NetConnection
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class NetConnectionModule {

    @Singleton
    @Provides
    fun provideNetConnectionModule(@ApplicationContext context: Context): NetConnection {
        return NetConnection(context)
    }
}