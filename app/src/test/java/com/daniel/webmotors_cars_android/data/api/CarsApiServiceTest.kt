package com.daniel.webmotors_cars_android.data.api

import com.google.common.truth.Truth
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.nio.charset.Charset

class CarsApiServiceTest {

    private lateinit var service: CarsApiService
    private lateinit var server: MockWebServer

    @Before
    fun setup() {
        server = MockWebServer()
        service = Retrofit.Builder()
            .baseUrl(server.url(""))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(CarsApiService::class.java)
    }

    private fun enqueueMockResponse(filename: String) {
        val inputStream = javaClass.classLoader!!.getResourceAsStream(filename)
        val source = inputStream.source().buffer()
        val mockResponse = MockResponse()
        mockResponse.setBody(source.readString(Charsets.UTF_8))
        server.enqueue(mockResponse)
    }

    @Test
    fun getCar_sentRequestPageOne_receivedExpected() {
        runBlocking {
            enqueueMockResponse("cars_response.json")
            val responseBody = service.getCars(1).body()
            val request = server.takeRequest()
            Truth.assertThat(responseBody).isNotNull()
            Truth.assertThat(request.path).isEqualTo("/api/OnlineChallenge/Vehicles?Page=1")
        }
    }

    @Test
    fun getCars_receiveResponsePageOne_correctContent() {
        runBlocking {
            enqueueMockResponse("cars_response.json")
            val responseBody = service.getCars(1).body()
            val carsList = responseBody!!
            val car = carsList[0]

            Truth.assertThat(car.price).isEqualTo("59000,00")
            Truth.assertThat(car.color).isEqualTo("Azul")
            Truth.assertThat(car.model).isEqualTo("City")
            Truth.assertThat(car.yearModel).isEqualTo(2018)
            Truth.assertThat(car.yearFab).isEqualTo(2017)
            Truth.assertThat(car.make).isEqualTo("Honda")
            Truth.assertThat(car.kM).isEqualTo(0)
        }
    }

    @After
    fun tearDown() {
        server.shutdown()
    }
}